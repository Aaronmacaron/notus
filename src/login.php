<?php
    session_start();
    require_once 'db.php';
    $showForm = true;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            if(isset($_POST["username"]) && isset($_POST["password"])){
                $username = '"' . $_POST["username"] . '"'; 
                $password = $_POST["password"];
                if($query = mysqli_query($db, "SELECT pwhash, id FROM users WHERE username = $username")){
                    $result = mysqli_fetch_array($query);
                    $hash = $result["pwhash"];
                    $id = $result["id"];
                    $isValid = password_verify($password, $hash);
                    if($isValid){
                        $_SESSION["userid"] = $id;
                        $_SESSION["username"] = $username;
                        echo "<p>Sie wurden erflolgreich eingeloggt!</p>";
                    }  else {
                        echo "<p style='color:red'>Das Passwort oder der Benutzername stimmt nicht.</p>";
                    }
                } else {
                    echo "<p>Es ist ein Fehler aufgetreten!</p>";
                }
            }
            
            if($showForm){
                ?>
                <form name="register" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <p>Benutzername:</p>
                    <input name='username'>

                    <p>Passwort:</p>
                    <input name='password' type="password">

                    <input type="submit">            
                </form>
        <?php
            }
        ?>
        
        <br>
        <br>
        
        <a href=".">Züruck zur Startseite</a>
    </body>
</html>
