<?php
    require_once 'db.php';
    $showForm = true;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            if(isset($_POST["username"]) && isset($_POST["password"])){
                $error = FALSE;
                $errorMessage = "";
                $username = $_POST["username"];
                $password = $_POST["password"];
                
                
                //Check if submitted data is valid
                if(strlen($username) <1){
                    $errorMessage .= "Bitte geben sie einen zulässigen Benutzernamen ein. <br> \n";
                    $error = true;
                }
                
                //Check if submitted data is valid
                if(strlen($password) <4){
                    $errorMessage .= "Bitte geben sie ein zulässiges Passwort ein. <br> \n";
                    $error = true;
                }
                
                //Check if username is available
                if(!$error){
                    $query = mysqli_query($db, "SELECT username FROM users WHERE username  = \"$username\"");
                    if(mysqli_num_rows($query) !=0){
                        $errorMessage .= "Dieser Benutzername wird schon verwendet. <br> \n";
                        $error = true;
                    }
                    mysqli_free_result($query);
                }
                
                //Register User
                if(!$error){
                    $hash = password_hash($password, PASSWORD_DEFAULT);
                    if($query = mysqli_query($db, "INSERT INTO users (username, pwhash) VALUES(\"$username\" , \"$hash\");")){
                        echo "Sie wurden erfolgreich regristriert! <br> \n";
                        $showForm = FALSE;
                    }  else {
                        $errorMessage .= "Ein Fehler ist aufgetreten.";
                        $error = true;
                    }
                }
                  
                
                if($error){
                    echo "<p style='color:red'>" . $errorMessage . "</p>";
                }
            }
            
            if($showForm){
                ?>
                <form name="register" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <p>Benutzername:</p>
                    <input name='username'>

                    <p>Passwort:</p>
                    <input name='password' type="password">

                    <input type="submit">            
                </form>
        <?php
            }
        ?>
        
        <br>
        <br>
        
        <a href=".">Züruck zur Startseite</a>
    </body>
</html>
